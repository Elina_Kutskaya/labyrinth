package com.example.labyrinth;

public enum MovementDirection {
    LEFT, UP, RIGHT, DOWN
}
