package com.example.labyrinth;

import android.view.GestureDetector;
import android.view.MotionEvent;

import static com.example.labyrinth.MovementDirection.DOWN;
import static com.example.labyrinth.MovementDirection.LEFT;
import static com.example.labyrinth.MovementDirection.RIGHT;
import static com.example.labyrinth.MovementDirection.UP;

public class GestureListener extends GestureDetector.SimpleOnGestureListener {
    private InputListener listener;

    GestureListener(InputListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        int diffX, diffY;
        diffX = Math.round(e2.getX() - e1.getX());
        diffY = Math.round(e2.getY() - e1.getY());
        if (Math.abs(diffX) > Math.abs(diffY)) {
            if (diffX > 0) {
                listener.onMove(RIGHT);
            } else {
                listener.onMove(LEFT);
            }
        } else {
            if (diffX > 0) {
                listener.onMove(UP);
            } else {
                listener.onMove(DOWN);
            }
        }
        return true;
    }
}
