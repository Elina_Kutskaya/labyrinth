package com.example.labyrinth;

public interface InputListener {
    void onMove(MovementDirection direction);
}
