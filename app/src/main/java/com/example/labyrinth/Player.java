package com.example.labyrinth;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Point;

/**
 * Класс игрока
 */
class Player extends FigureCreation {

    Player(Context context, Point start, int size) {
        super(size, start, getPaint(context,2,R.color.gm_player, Paint.Style.FILL));

    }
}
