package com.example.labyrinth;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;

import java.util.Objects;

public class FigureCreation implements Drawable {
    protected Point point;
    private Paint paint;
    private int size;

    FigureCreation(int size, Point point, Paint paint) {
        this.size = size;
        this.point = point;
        this.paint = paint;
    }

    public Point getPoint() {
        return point;
    }

    void goTo(int x, int y) {
        point.x = x;
        point.y = y;
    }

    int getX() {
        return point.x;
    }

    int getY() {
        return point.y;
    }

    public static Paint getPaint(Context context, int width, int color, Paint.Style style) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(width);
        paint.setStyle(style);
        paint.setColor(ContextCompat.getColor(context, color));
        return paint;
    }
    @Override
    public void draw(Canvas canvas, Rect rect) {
        float cellSize = (float) (rect.right - rect.left) / size;
        canvas.drawRect(
                rect.left + point.x * cellSize,
                rect.top + point.y * cellSize,
                rect.left + point.x * cellSize + cellSize,
                rect.top + point.y * cellSize + cellSize,
                paint);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof FigureCreation)) return false;
        FigureCreation figureCreation = (FigureCreation) obj;
        return size == figureCreation.size &&
                Objects.equals(point, figureCreation.point);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {

        return Objects.hash(size, point, paint);
    }
}
