package com.example.labyrinth;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class GameManager extends View implements InputListener {
    private List<Drawable> drawables = new ArrayList<>();
    private Context context;
    private View view;
    private Player player;
    private Maze maze;
    private Rect rect = new Rect();
    private long startTime;
    private long endTime;
    private Canvas canvas;
    private Paint paint = new Paint();

    public GameManager(Context context) {
        super(context);
        this.context = context;
        startTime = System.currentTimeMillis();
        create(5);
    }

    /**
     * вызывает генератор лабиринта (maze = new Maze(context, size);)
     * <p>
     * записывает готовый лабиринт в ArrayList (drawables.add(maze);)
     * <p>
     * создает пользователя и добовляет его на игровое поле
     *
     * @param size первоночальный размер лабиринта
     */
    private void create(int size) {
        drawables.clear();

        maze = new Maze(context, size);
        drawables.add(maze);
        player = new Player(context, maze.getStart(), size);
        drawables.add(player);
    }

    /**
     * проверяет возможность перемещения
     * передвигает персонажа в выбранное напревление
     *
     * @param direction движения
     */
    @Override
    public void onMove(MovementDirection direction) {
        int diffX = 0;
        int diffY = 0;

        switch (direction) {
            case LEFT:
                diffX = -1;
                break;
            case UP:
                diffY = -1;
                break;
            case RIGHT:
                diffX = 1;
                break;
            case DOWN:
                diffY = 1;
                break;
        }

        int stepX = player.getX();
        int stepY = player.getY();

        while (maze.canPlayerGoTo(stepX + diffX, stepY + diffY)) {
            stepX += diffX;
            stepY += diffY;
            if (diffX != 0) {
                if (maze.canPlayerGoTo(stepX, stepY + 1)
                        || maze.canPlayerGoTo(stepX, stepY - 1)) {
                    break;
                }
            }
            if (diffY != 0) {
                if (maze.canPlayerGoTo(stepX + 1, stepY)
                        || maze.canPlayerGoTo(stepX - 1, stepY)) {
                    break;
                }
            }
        }
        if (maze.getSize() <= 50) {
            player.goTo(stepX, stepY);
            if (player.getX() == 0 || player.getY() == 0) {
                create(maze.getSize() + 5);
            }
            endTime = System.currentTimeMillis();
            view.invalidate();
        } else {
            create(5);
            startTime = System.currentTimeMillis();
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        for (Drawable drawableItem :
                drawables) {
            drawableItem.draw(canvas, rect);
            paint.setTextSize(30);
            paint.setColor(Color.WHITE);
            canvas.drawText("Time: " + timer(), 0, 30, paint);
        }
    }

    /**
     * Таймер
     */
    public String timer() {
        long time = endTime - startTime;
        int min = (int) time / 60000;
        int sec = (int) time % 60000 / 1000;
        String timeLeftText;
        timeLeftText = "" + min + ":";
        if (sec < 10) timeLeftText += "0";
        timeLeftText += sec;
        return timeLeftText;
    }

    public void setView(View view) {
        this.view = view;
    }

    /**
     * Актуальные размеры полотна
     *
     * @param width  ширина
     * @param height высота
     */
    public void setScreenSize(int width, int height) {
        int screenSize = Math.min(width, height);
        rect.set(
                (width - screenSize) / 2,
                (height - screenSize) / 2,
                (width + screenSize) / 2,
                (height + screenSize) / 2
        );
    }
}
