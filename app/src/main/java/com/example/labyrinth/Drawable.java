package com.example.labyrinth;

import android.graphics.Canvas;
import android.graphics.Rect;

interface Drawable {
    void draw(Canvas canvas, Rect rect);
}
