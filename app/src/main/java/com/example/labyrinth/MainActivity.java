package com.example.labyrinth;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;

import static com.example.labyrinth.MovementDirection.DOWN;
import static com.example.labyrinth.MovementDirection.LEFT;
import static com.example.labyrinth.MovementDirection.RIGHT;
import static com.example.labyrinth.MovementDirection.UP;

public class MainActivity extends AppCompatActivity {

    private GestureDetector gestureDetector;
    private InputListener inputListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GameManager gameManager = new GameManager(this);
        inputListener = gameManager;
        MazeView view = new MazeView(this, gameManager);
        setContentView(view);
        gestureDetector = new GestureDetector(this, new GestureListener(gameManager));
    }

    /**
     * Обработка касания
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                inputListener.onMove(LEFT);
                return true;
            case KeyEvent.KEYCODE_DPAD_UP:
                inputListener.onMove(UP);
                return true;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                inputListener.onMove(RIGHT);
                return true;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                inputListener.onMove(DOWN);
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
